# Play Dice

Play dice with 2 player.

## Summery

Version :
1. Android Studio 4.1.1

## How it works:

On click of *Roll the dice*, get new number with dice sound.


![](https://media.giphy.com/media/SjV9phLFtdL0pwlQxz/giphy.gif)


### Installation

Clone this repository and import into Android Studio

1. Open PlatDice app in the Android studio.
2. Run an App on the simulator or device

### Dependencies

com.daimajia.androidanimations:library:2.4@aar (for Animation)








