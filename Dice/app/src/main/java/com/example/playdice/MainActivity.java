package com.example.playdice;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView dice1;
    private ImageView dice2;
    private Button button;
    private int [] diceImages;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        playDice();
    }

    void initView(){
         dice1 = findViewById(R.id.imgDice1);
         dice2 = findViewById(R.id.imgDice2);
         button = findViewById(R.id.btnRollTheDice);
         diceImages = new int[]{R.drawable.dice1, R.drawable.dice2, R.drawable.dice3, R.drawable.dice4, R.drawable.dice5, R.drawable.dice6};
         mediaPlayer = MediaPlayer.create(this,R.raw.dice_sound);
    }

    void playDice(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Generate random number
                Random random = new Random();
                int randomNum = random.nextInt(5); //0 .. 5
                dice1.setImageResource(diceImages[randomNum]);

                randomNum = random.nextInt(5); //0 .. 5
                dice2.setImageResource(diceImages[randomNum]);

                //Animation
                YoYo.with(Techniques.Shake)
                        .duration(400)
                        .repeat(0)
                        .playOn(dice1);

                YoYo.with(Techniques.Shake)
                        .duration(400)
                        .repeat(0)
                        .playOn(dice2);

                mediaPlayer.start();
            }
        });
    }
}